#include <QGuiApplication>
#include <QQmlApplicationEngine>
//#include <qtwebengineglobal.h>

int main(int argc, char *argv[])
{
    QCoreApplication::setOrganizationName("QtExamples");
    QCoreApplication::setAttribute(Qt::AA_ShareOpenGLContexts);
    //QtWebEngine::initialize();
    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;
    engine.addImportPath("qrc:/qt/qml/");
    engine.load(QUrl(QStringLiteral("qrc:/qt/qml/testWebengine/main.qml")));

    return app.exec();
}
